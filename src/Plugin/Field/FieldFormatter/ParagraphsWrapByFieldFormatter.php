<?php

namespace Drupal\paragraphs_wrap_by_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsEntityFormatter;
use Drupal\Core\Render\Element;

/**
 * Plugin implementation of the 'Paragraphs wrapper' formatter.
 *
 * @FieldFormatter(
 *   id = "paragraphs_wrap_by_field",
 *   label = @Translation("Paragraphs wrap by field"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ParagraphsWrapByFieldFormatter extends EntityReferenceRevisionsEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'wrapping_field' => '',
    ] + parent::defaultSettings();
  }

  /**
   *
   */
  public function getListStringFields() {

    $list_string_fields = [];

    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('paragraph');
    foreach ($bundles as $key => $bundle) {
      $fieldDefinition = \Drupal::service('entity_field.manager')->getFieldDefinitions('paragraph', $key);
      foreach ($fieldDefinition as $field_definition) {

        if ($field_definition->getType() == 'list_string') {
          $list_string_fields[$field_definition->getName()] = $field_definition->getLabel();
        }
      }
    }

    return $list_string_fields;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form = parent::settingsForm($form, $form_state);

    $form['wrapping_field'] = [
      '#type' => 'select',
      '#options' => $this->getListStringFields(),
      '#title' => $this->t('Field to check value for wrapping'),
      '#default_value' => $this->getSetting('wrapping_field'),
      '#required' => FALSE,
      '#description' => $this->t('If the value of this field is the same in any of the paragraphs in a row, then they will be wrapped in a div with a class equal to the value of the field. (Only list_field types are listed)'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->getSetting('wrapping_field') ? t('Wrapping field: ') . $this->getSetting('wrapping_field') : t('No wrapping');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {

    $wrapping_field = $this->getSetting('wrapping_field');

    $groupCount = 0;
    $itemCount = 0;
    $view = parent::view($items, $langcode);
    $paragraphWraps = [];
    $lastParagraph = FALSE;
    foreach (Element::children($view) as $childKey) {
      $el = $view[$childKey];
      /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
      $paragraph = $el['#paragraph'];
      // If we've been through the loop onces and this paragraph is of the same
      // bundle as the last one then add it to that group.
      if (
        $itemCount > 0
        && $lastParagraph
        && ($paragraph->{$wrapping_field}->value == $lastParagraph->{$wrapping_field}->value)
      ) {
        $lastEl = &$paragraphWraps[($groupCount - 1)];
        $lastEl['#items'][] = $el;
      }
      // New group
      else {
        /** @var \Drupal\node\Entity\Node $parent */
        $parent = $items->getParent()->getValue();
        $paragraphWraps[$groupCount] = [
          '#theme' => 'paragraphs_wrap_by_field_wrap',
          '#wrap_value' => $paragraph->{$wrapping_field}->value,
          '#parent' => $parent,
          '#items' => [$el],
        ];
        $groupCount++;
      }
      $itemCount++;
      $lastParagraph = $paragraph;
    }
    unset($view['#items']);
    $view['#theme'] = 'paragraphs_wrap_by_field_container';
    $view['#wraps'] = $paragraphWraps;
    return $view;
  }

}
